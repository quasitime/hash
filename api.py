import hashlib
import base64
import time
from time import sleep
from flask import Flask, request, abort, jsonify
app = Flask(__name__)

n=0  #total number of hash requests
t=0  #totel process time
q=0  #queue counter


#Shutdown Function
def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running')
    func()

#Index Route
@app.route('/')

def index():
  return 'Alive'

#Hash Route
@app.route('/hash', methods=['GET','POST'])

def hash():
  global q
  global t
  global n

  if q > 50:                            
    abort(403, 'Too Many Requests')  #Serve a response if there are too many requests.
  start = time.process_time()
  
  q+=1
  n+=1

  if request.method == 'POST':
    password = request.form['password']                            #Grab the password 
    h = hashlib.sha512(str(password).encode('utf-8')).hexdigest()  #Call hash function
    b64 = base64.b64encode(bytes(h,'utf-8'))                       #Base64 encode it
    sleep(5)
    print (str(q) + '\n')
    q-=1
    t += time.process_time() - start
    return b64
    
  elif request.method == 'GET':
    return 'Still Alive'

#Shutdown Route  
@app.route('/shutdown', methods=['GET','POST'])

def shutdown():
  while q > 0:                                                     #Sleep while the queue is still going.
    sleep(1)
  shutdown_server()
  return 'Shutting down...'

#Stats Route  
@app.route('/stats', methods=['GET'])

def stats():
  if n > 0:
    avg = t / n
    return jsonify(total=str(n), average_t=str(avg * 1000), total_t=str(t * 1000))  #Present status in JSON
  else:
    return 'Where the hashes be?'
    