Requirements:

**1) . Flask**

**2) . Python 3.7**


Steps to deploy:

1) . Create your virtual environment

**mkdir sandbox**

**cd sandbox**

**python3 -m venv env1**


2) . Activate it

**. ./env1/bin/activate**

3) . Install Flask & set environment variables

**pip install Flask**

**export FLASK_APP=api.py**

**export FLASK_ENV=development**


4) . Clone the repo.

**git clone https://bitbucket.org/quasitime/hash.git**

5) . Change directory & run the application

**cd hash  (make sure you are in the directory with api.py)**

**flask run**

5) . Access the API at http://127.0.0.1:5000/

6) . Endpoints are: 

**/**

**/hash**

**/shutdown**

**/stats**

